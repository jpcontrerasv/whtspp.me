$(document).ready(function () {

    //Comparision slider
    $(".twentytwenty-container").twentytwenty({
        default_offset_pct: 0.18, // How much of the before image is visible when the page loads
        orientation: 'horizontal', // Orientation of the before and after images ('horizontal' or 'vertical')
        before_label: '', // Set a custom before label
        after_label: '', // Set a custom after label
        no_overlay: false, //Do not show the overlay with before and after
        move_slider_on_hover: true, // Move slider on mouse hover?
        move_with_handle_only: true, // Allow a user to swipe anywhere on the image to control slider movement. 
        click_to_move: true // Allow a user to click (or tap) anywhere on the image to move the slider to that location.
    });

    //rotate text
    var replace = $('.replace-me').ReplaceMe({
        animation: 'animated fadeInUp',
        speed: 2500,
        separator: ','
    });



    //International tel input
    var inputMain = document.querySelector("#phone");
    if ($("#phoneB").length > 0) {
        var inputSec = document.querySelector("#phoneB");

        window.intlTelInput(inputSec, {
            // allowDropdown: false,
            // autoHideDialCode: false,
            autoPlaceholder: "off",
            // dropdownContainer: document.body,
            // excludeCountries: ["us"],
            // formatOnDisplay: false,
            // geoIpLookup: function(callback) {
            //   $.get("http://ipinfo.io", function() {}, "jsonp").always(function(resp) {
            //     var countryCode = (resp && resp.country) ? resp.country : "";
            //     callback(countryCode);
            //   });
            // },
            // hiddenInput: "full_number",
            // initialCountry: "auto",
            // localizedCountries: { 'de': 'Deutschland' },
            // nationalMode: false,
            // onlyCountries: ['us', 'gb', 'ch', 'ca', 'do'],
            // placeholderNumberType: "MOBILE",
            // preferredCountries: ['cn', 'jp'],
            separateDialCode: true,
            initialCountry: "auto",
            geoIpLookup: function (success, failure) {
                $.get("https://ipinfo.io?token=e481bc6d183087", function () { }, "json").always(function (resp) {
                    var countryCode = (resp && resp.country) ? resp.country : "es";
                    success(countryCode);
                });
            },
            //initialCountry: "auto",
            utilsScript: "public/js/utils.js",
        });
    }

    window.intlTelInput(inputMain, {
        // allowDropdown: false,
        // autoHideDialCode: false,
        autoPlaceholder: "off",
        // dropdownContainer: document.body,
        // excludeCountries: ["us"],
        // formatOnDisplay: false,
        // geoIpLookup: function(callback) {
        //   $.get("http://ipinfo.io", function() {}, "jsonp").always(function(resp) {
        //     var countryCode = (resp && resp.country) ? resp.country : "";
        //     callback(countryCode);
        //   });
        // },
        // hiddenInput: "full_number",
        // initialCountry: "auto",
        // localizedCountries: { 'de': 'Deutschland' },
        // nationalMode: false,
        // onlyCountries: ['us', 'gb', 'ch', 'ca', 'do'],
        // placeholderNumberType: "MOBILE",
        // preferredCountries: ['cn', 'jp'],
        separateDialCode: true,
        initialCountry: "auto",
        geoIpLookup: function (success, failure) {
            $.get("https://ipinfo.io?token=e481bc6d183087", function () { }, "json").always(function (resp) {
                var countryCode = (resp && resp.country) ? resp.country : "es";
                success(countryCode);
            });
        },
        //initialCountry: "auto",
        utilsScript: "public/js/utils.js",
    });




    //toggle checkbox
    $(".helloMessage").hide();
    $("#checkMessage").click(function () {
        $(".helloMessage").toggle();
    });

    // Comprobar disponibilidad
    $("#check-availability").click(function () {
        console.log('clicking');
        var enlace = $("#field-san").val();
        // se valida que el campo esté lleno
        if (enlace !== "") {
            $("#loader-b").css("display", "block");
            $.ajax({
                url: "usuario",
                type: 'POST',
                data: {
                    enlace: enlace
                },
                beforeSend: function () {
                    console.log("aqui se habilita el loader");
                },
                success: function (response) {
                    $("#loader-b").css("display", "none");
                    if (response === "disponible") {
                        $(".main-cta").attr('disabled', false);
                        $("#alert-vacio").css('display', 'none');
                        $("#alert-nodisp").css('display', 'none');
                        $("#alert-disp").css('display', 'block');
                    }
                    if (response === "no-disponible") {
                        $(".main-cta").attr('disabled', true);
                        $("#alert-vacio").css('display', 'none');
                        $("#alert-disp").css('display', 'none');
                        $("#alert-nodisp").css('display', 'block');
                    }
                }
            });
        } else {
            $("#alert-vacio").css('display', 'block');
            $("#alert-disp").css('display', 'none');
            $("#alert-nodisp").css('display', 'none');
        }
    });

    // Comprar
    $(".main-cta").click(function () {
        console.log('clicking paypal');
        var enlace = $("#field-san").val();
        var codigo = $(".iti__selected-dial-code").html().split("+")[1];
        var numero = $("#phone").val();
        console.log(enlace + " - ( " + codigo + " ) " + numero);
        // se valida que el campo esté lleno
        if (enlace !== "") {
            $("#loader-b").css("display", "block");
            $.ajax({
                url: "paypal",
                type: 'POST',
                data: {
                    enlace: enlace,
                    codigo: codigo,
                    numero: numero
                },
                beforeSend: function () {
                    console.log("aqui se habilita el loader");
                },
                success: function (response) {
                    $("#loader-b").css("display", "none");
                    $("#resp-paypal").html(response);
                    $('input[name="pp_submit"]').trigger('click');
                    /*if(response === "disponible"){
                        $(".main-cta").attr('disabled',false);
                        $("#alert-vacio").css('display','none');
                        $("#alert-nodisp").css('display','none');
                        $("#alert-disp").css('display','block');
                    }
                    if(response === "no-disponible"){
                        $(".main-cta").attr('disabled',true);
                        $("#alert-vacio").css('display','none');
                        $("#alert-disp").css('display','none');
                        $("#alert-nodisp").css('display','block');
                    }*/
                }
            });
        }
    });


    // enviar enlace al correo
    $(".send-log").click(function () {
        //console.log('clicking mail');
        var correo = $("#correo-user").val();

        console.log(correo);
        // se valida que el campo esté lleno
        if (correo !== "") {
            $("#loader-b").css("display", "block");
            $.ajax({
                url: "usuario/login",
                type: 'POST',
                data: {
                    correo: correo
                },
                beforeSend: function () {
                    //console.log("aqui se habilita el loader");
                },
                success: function (response) {
                    $("#loader-b").css("display", "none");
                    $('#ifMailExist').show().removeClass("d-none");
                    $("#ifMailEmpty").hide().addClass("d-none");;
                    /*if(response === "disponible"){
                        $(".main-cta").attr('disabled',false);
                        $("#alert-vacio").css('display','none');
                        $("#alert-nodisp").css('display','none');
                        $("#alert-disp").css('display','block');
                    }
                    if(response === "no-disponible"){
                        $(".main-cta").attr('disabled',true);
                        $("#alert-vacio").css('display','none');
                        $("#alert-disp").css('display','none');
                        $("#alert-nodisp").css('display','block');
                    }*/
                }
            });
        }
        else {
            $("#ifMailEmpty").show().removeClass("d-none");
        }
    });

    //Esconder advertencia de correo enviado
    $('#ifMailExist').hide().addClass("d-none");
    $('#ifMailEmpty').hide().addClass("d-none");

    //Esconder advertencias de correo y limpiar input enviado al cerrar modal
    $('#enterModal').on('hidden.bs.modal', function () {
        $('#ifMailExist').hide().addClass("d-none");
        $("#ifMailEmpty").hide().addClass("d-none");
        $('#correo-user').val('');
    });
    $('button.btn-close').click(function () {
        $('#ifMailExist').hide().addClass("d-none");
        $("#ifMailEmpty").hide().addClass("d-none");
        $('#correo-user').val('');
    });




    // abrir modal editar (con data)
    $(".editar").click(function () {
        console.log('clicking edit');
        var id_compra = $(this).attr('id').split("-")[1];
        console.log(id_compra);
        // se valida que el campo esté lleno
        if (id_compra !== "") {
            $("#loader-b").css("display", "block");
            $.ajax({
                url: "dashboard/getBuyId",
                type: 'POST',
                data: {
                    id_compra: id_compra
                },
                beforeSend: function () {
                    console.log("aqui se habilita el loader");
                },
                success: function (response) {
                    $("#loader-b").css("display", "none");
                    $("#body-modal-edit").html(response);
                    if ($("#phoneB").length > 0) {
                        var inputSec = document.querySelector("#phoneB");

                        window.intlTelInput(inputSec, {
                            // allowDropdown: false,
                            // autoHideDialCode: false,
                            autoPlaceholder: "off",
                            // dropdownContainer: document.body,
                            // excludeCountries: ["us"],
                            // formatOnDisplay: false,
                            // geoIpLookup: function(callback) {
                            //   $.get("http://ipinfo.io", function() {}, "jsonp").always(function(resp) {
                            //     var countryCode = (resp && resp.country) ? resp.country : "";
                            //     callback(countryCode);
                            //   });
                            // },
                            // hiddenInput: "full_number",
                            // initialCountry: "auto",
                            // localizedCountries: { 'de': 'Deutschland' },
                            // nationalMode: false,
                            // onlyCountries: ['us', 'gb', 'ch', 'ca', 'do'],
                            // placeholderNumberType: "MOBILE",
                            // preferredCountries: ['cn', 'jp'],
                            separateDialCode: true,
                            initialCountry: "auto",
                            /*geoIpLookup: function (success, failure) {
                                $.get("https://ipinfo.io?token=e481bc6d183087", function () { }, "json").always(function (resp) {
                                    var countryCode = (resp && resp.country) ? resp.country : "es";
                                    success(countryCode);
                                });}, */
                            //initialCountry: "auto",
                            utilsScript: "public/js/utils.js",
                        });
                    }
                    $(".phone-slug").stringToSlug({
                        options: {
                            custom: {
                                '.': '.',
                                'ñ': 'ñ'
                            }
                        }
                    });
                    $("#fieldB").stringToSlug({
                        getPut: '.permalinkB',
                        options: {
                            custom: {
                                '.': '.',
                                'ñ': 'ñ'
                            }
                        }
                    });
                }
            });
        }
    });

    // Comprobar disponibilidad
    $(document).on('click', "#check-availability-edit", function () {
        console.log('clicking edit check');
        var enlace = $("#fieldB-san").val();
        var id_compra = $("#id_compra_editar").val();
        // se valida que el campo esté lleno
        if (enlace !== "") {
            $("#loader-b").css("display", "block");
            $.ajax({
                url: "dashboard/getDisponibilidad",
                type: 'POST',
                data: {
                    enlace: enlace,
                    id_compra: id_compra
                },
                beforeSend: function () {
                    console.log("aqui se habilita el loader");
                },
                success: function (response) {
                    $("#loader-b").css("display", "none");
                    if (response === "disponible") {
                        $(".main-cta").attr('disabled', false);
                        $("#alert-vacio-edit").css('display', 'none');
                        $("#alert-nodisp-edit").css('display', 'none');
                        $("#alert-disp-edit").css('display', 'block');
                    }
                    if (response === "no-disponible") {
                        $(".main-cta").attr('disabled', true);
                        $("#alert-vacio-edit").css('display', 'none');
                        $("#alert-disp-edit").css('display', 'none');
                        $("#alert-nodisp-edit").css('display', 'block');
                    }
                }
            });
        } else {
            $("#alert-vacio-edit").css('display', 'block');
            $("#alert-disp-edit").css('display', 'none');
            $("#alert-nodisp-edit").css('display', 'none');
        }
    });


    // Editar function
    $(".editar_registro").click(function () {
        console.log('clicking editar registro');
        var enlace = $("#fieldB-san").val();
        var codigo = $("#phoneBIti .iti__selected-dial-code").html().split("+")[1];
        var numero = $("#phoneB").val();
        var id_compra = $("#id_compra_editar").val();
        console.log(enlace + " - ( " + codigo + " ) " + numero);
        // se valida que el campo esté lleno
        if (enlace !== "") {
            $("#loader-b").css("display", "block");
            $.ajax({
                url: "dashboard/editBuy",
                type: 'POST',
                data: {
                    enlace: enlace,
                    codigo: codigo,
                    numero: numero,
                    id_compra: id_compra
                },
                beforeSend: function () {
                    console.log("aqui se habilita el loader");
                },
                success: function (response) {
                    $("#loader-b").css("display", "none");
                    console.log(response);
                    location.reload();
                    /*if(response === "disponible"){
                        $(".main-cta").attr('disabled',false);
                        $("#alert-vacio").css('display','none');
                        $("#alert-nodisp").css('display','none');
                        $("#alert-disp").css('display','block');
                    }
                    if(response === "no-disponible"){
                        $(".main-cta").attr('disabled',true);
                        $("#alert-vacio").css('display','none');
                        $("#alert-disp").css('display','none');
                        $("#alert-nodisp").css('display','block');
                    }*/
                }
            });
        }
    });
    $(".enlace").click(function () {
        var id = $(this).attr('id').split("-")[1];
        var valor = $("#enlace-" + id).text();
        console.log(id + " - " + valor);
        $("#addressWsp").html(valor);
        $("#addressWsp").attr("href", valor);
    });
    // abrir modal eliminar
    $(".eliminar").click(function () {
        console.log('clicking edit');
        var id_compra = $(this).attr('id').split("-")[1];
        console.log(id_compra);
        // se valida que el campo esté lleno
        if (id_compra !== "") {
            $("#loader-b").css("display", "block");
            $.ajax({
                url: "dashboard/getBuyIdDelete",
                type: 'POST',
                data: {
                    id_compra: id_compra
                },
                beforeSend: function () {
                    console.log("aqui se habilita el loader");
                },
                success: function (response) {
                    $("#loader-b").css("display", "none");
                    $("#body-modal-delete").html(response);
                }
            });
        }
    });
    // Delete action
    $(".eliminar_registro").click(function () {
        console.log('clicking delete registro');
        var id_compra = $("#id_compra_eliminar").val();
        // se valida que el campo esté lleno
        $("#loader-b").css("display", "block");
        $.ajax({
            url: "dashboard/deleteBuy",
            type: 'POST',
            data: {
                id_compra: id_compra
            },
            beforeSend: function () {
                console.log("aqui se habilita el loader");
            },
            success: function (response) {
                $("#loader-b").css("display", "none");
                console.log(response);
                location.reload();
                /*if(response === "disponible"){
                    $(".main-cta").attr('disabled',false);
                    $("#alert-vacio").css('display','none');
                    $("#alert-nodisp").css('display','none');
                    $("#alert-disp").css('display','block');
                }
                if(response === "no-disponible"){
                    $(".main-cta").attr('disabled',true);
                    $("#alert-vacio").css('display','none');
                    $("#alert-disp").css('display','none');
                    $("#alert-nodisp").css('display','block');
                }*/
            }
        });
    });
    //Modal abierto
    //$('#deleteModal').modal('show');



    //String to slug
    $(".phone-slug").stringToSlug({
        options: {
            custom: {
                '.': '.',
                'ñ': 'ñ',
            }
        }
    });
    //dominio.cl<>!"%&//(

    $("#fieldB").stringToSlug({
        getPut: '.permalinkB',
        options: {
            custom: {
                '.': '.',
                'ñ': 'ñ'
            }
        }
    });

    //Copiar URL
    new ClipboardJS('.btn');

});

//Contador de caracteres
function countChar(val) {
    var len = val.value.length;
    if (len === 0) {
        $(".main-cta").attr('disabled', true);
    }
    if (len >= 35) {
        val.value = val.value.substring(0, 35);
    } else {
        $('#charNum').text(35 - len);
    }
}


//Contador de caracteres - ya comprado
function countCharB(val) {
    var len = val.value.length;
    if (len === 0) {
        $("#save-change").attr('disabled', true);
    }
    if (len >= 35) {
        val.value = val.value.substring(0, 35);
    } else {
        $('#charNumB').text(35 - len);
    }
}
