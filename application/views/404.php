<header class="site-header sticky-top py-1 bg-persian">
  <nav class="container-fluid d-flex flex-row align-items-center justify-content-center">
    <a class="py-2" href="<?php echo base_url(); ?>" aria-label="Product">
      <img src="<?php echo base_url('/public/img/whtsppmelogo.svg'); ?>" alt="wsp.chat logo">
    </a>
  </nav>
</header>

<div class="container pt-4 mt-4">
  <div class="row d-flex justify-content-center">


    <!--left call-->
    <div class="left-call col-xl-10 col-lg-10 col-md-12 col-sm-12 col-12">



      <div class="alert alert-warning text-center" role="alert">
        <h1 class="display-5 fw-normal m-0">
          https://wsp.chat/<?php echo $url; ?>
        </h1>
      </div>
      <div class="w-100 text-center">
        <h2>No se encuentra disponible</h2>
        <p>Pero lo puedes registrar para que sea una dirección única e inolvidable.</p>
        <p><a href="<?php echo base_url(); ?>"> Volver al home.</a></p>
      </div>




    </div>


  </div>
</div>