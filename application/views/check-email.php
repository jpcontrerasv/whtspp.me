<header class="site-header sticky-top py-1 bg-persian">
  <nav class="container-fluid d-flex align-items-center justify-content-center">
    <a class="py-2" href="<?php echo base_url(); ?>" aria-label="Product">
      <img src="<?php echo base_url('/public/img/whtsppmelogo.svg'); ?>" alt="wsp.chat logo">
    </a>
  </nav>
</header>

<div class="container">
  <div class="row">

    <div class="col-12 text-center">
      <div class="alert alert-success m-5" role="alert">
        <h4 class="alert-heading"><?php echo lang('CHECKPAGE_TITLE'); ?></h4>
        <p><?php echo lang('CHECKPAGE_BODY'); ?></p>
        <hr>
        <p class="mb-0"><strong><small> <?php echo lang('CHECKPAGE_FOOTER'); ?></small></strong></p>
        <a href="<?php echo base_url(); ?>"><strong><small> <?php echo lang('BACK_TO_INDEX'); ?></small></strong> </a>
      </div>
    </div>

  </div>
</div>