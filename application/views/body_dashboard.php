  <div class="container-fluid mt-4">

    <!--dashboard-->
    <div class="row">

      <!--Tabla direcciones-->
      <div class="col-xl-8 col-lg-7 col-md-12 col-sm-12 col-12 mb-4">

        <div class="card">
          <div class="card-header bg-mystic">
            <h5 class="m-0 fw-normal fw-bold"><?php echo lang('MY_ADDRESSES'); ?></h5>
          </div>
          <div class="card-body bg-pradera">
            <div class="col-12">
              <div id="user_address" class="table-responsive">
                <table class="table">
                  <thead>
                    <tr>
                      <th scope="col"><?php echo lang('TH_COPY'); ?></th>
                      <th><?php echo lang('TH_URL'); ?></th>
                      <th><?php echo lang('TH_CLICKS'); ?></th>
                      <th><?php echo lang('TH_PHONE'); ?></th>
                      <th><?php echo lang('TH_EXP'); ?></th>
                      <th><?php echo lang('TH_EDIT'); ?></th>
                      <th><?php echo lang('TH_DELETE'); ?></th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php foreach ($compras as $comp) {
                      # code...
                    ?>
                      <tr class="comp-<?php echo $comp->id_compra; ?>">
                        <td scope="row"><button class="btn btn-primary btn-sm enlace" data-bs-toggle="modal" data-bs-target="#helloModal"  id="copy-<?php echo $comp->id_compra; ?>" ><?php echo lang('TH_COPY'); ?></button>
                        </td>
                        <td><small><a href="<?php echo $comp->dominio . $comp->enlace;  ?>" id="enlace-<?php echo $comp->id_compra; ?>" rel="noopener noreferrer" target="_blank" ><?php echo $comp->dominio . $comp->enlace;  ?></small></a></td>
                        <td><small><?php echo $comp->visitas;  ?></small></td>
                        <td><small><?php echo "+" . $comp->codigo_numero . ' ' . $comp->numero_asociado;  ?></small></td>
                        <td><small><?php echo date('d/m/Y', strtotime($comp->f_vence)); ?></small></td>
                        <td><small><a href="#" data-bs-toggle="modal" data-bs-target="#editModal" class="editar" id="edit-<?php echo $comp->id_compra; ?>"><?php echo lang('TH_EDIT'); ?></a></small></td>
                        <td><small><a href="#" class="text-danger eliminar" data-bs-toggle="modal" data-bs-target="#deleteModal" id="delete-<?php echo $comp->id_compra; ?>"><?php echo lang('TH_DELETE'); ?></a></small></td>
                      </tr>
                    <?php  }  ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>

      <!--creador de url-->
      <div class="col-xl-4 col-lg-5 col-md-12 col-sm-12 col-12 mb-4">
        <div class="card border-0 main-action bg-pradera mb-3">
          <div class="card-header bg-mystic">
            <h5 class="m-0 fw-normal fw-bold"><?php echo lang('SIDE_TITLE_SECTION'); ?></h5>
          </div>

          <div class="card-body">

            <!-- 1 -->
            <section class="border-light border-bottom pb-3 mb-4">
              <p class="fs-5 fw-light"><?php echo lang('TITLE_WHATSAPP_NUMBER'); ?></p>
              <input id="phone" class="w-100" name="phone" type="tel">

            </section>

            <!-- 2 -->
            <section class="border-light mb-2">
              <p style="line-height: 1.1em;" class="fs-4"><?php echo lang('TITLE_CREATE_ADDRESS'); ?></p>

              <div class="row">

                <div class="col-xl-7 col-lg-7 col-md-12 col-sm-12 col-12">
                  <input id="field" onkeyup="countChar(this)" type="text" class="phone-slug" maxlength="35" />
                  <div id="charNum" class="px-2">35</div>
                  <div class="permalink-desk">
                    <small>wsp.chat/<span id="perml" class="permalink"></span></small>
                        <input id="field-san" type="text" class="permalink" value="" hidden/>
                  </div>
                </div>

                <div class="col-xl-5 col-lg-5 col-md-12 col-sm-12 col-12">

                  <div class="check-available w-100 text-center">
                    <button type="button" class="btn btn-success btn-available mx-auto w-100" id="check-availability"><?php echo lang('AVAILABILTY_BUTTON'); ?></button>
                  </div>
                  <div id="resp-paypal" style="display:none"></div>
                </div>

              </div>



              <div class="d-flex flex-column justify-content-center">
                <div class="alert alert-success text-center mb-1 p-2" role="alert" style="display: none;" id="alert-disp">
                  <?php echo lang('AVAILABILTY_YES'); ?>
                </div>
                <div class="alert alert-danger text-center mb-1 p-2" role="alert" style="display: none;" id="alert-vacio">
                  <?php echo lang('EMPTY_FIELD'); ?>
                </div>
                <div class="alert alert-danger text-center mb-1 p-2" role="alert" style="display: none;" id="alert-nodisp">
                  <?php echo lang('AVAILABILTY_NO'); ?>
                </div>
              </div>


            </section>


            <?php /*
            
            <!--3-->
            <section class="pb-1 mb-1 pt-2 border-light border-top">
              <div class="mb-3 form-check">
                <input type="checkbox" class="form-check-input" id="checkMessage">
                <label class="form-check-label" for="checkMessage">&nbsp;&nbsp;<?php echo lang('INCLUDE_MESSAGE'); ?></label>
              </div>
              <div class="mb-3">
                <div class="form-group helloMessage">
                  <textarea class="form-control" placeholder="<?php echo lang('PLACEHOLDER_MESSAGE'); ?>" rows="3" maxlength="140"></textarea>
                </div>
              </div>
            </section>

            */ ?>


            <!-- 4 -->
            <section class="text-center pb-1 mb-1 mt-5">
              <button class="btn main-cta mb-3" disabled>
                <?php echo lang('GET_IT_BUTTON'); ?>
              </button>
              <div class="logo-paypal w-100 text-muted">
                🔒 <?php echo lang('SAFE_PAYMENT'); ?> &nbsp; <img src="<?php echo base_url('/public/img/pplogo.svg'); ?>">
              </div>
              <?php /*
              <p class="m-0 text-muted" style="line-height: 1em;"><small><?php echo lang('GET_IT_DISCLAIMER'); ?></small> </p>
              */ ?>
            </section>





          </div>
        </div>
      </div>

    </div>



  </div>


  <!-- Modal Bievenida -->
  <div class="modal fade" id="helloModal" tabindex="-1" aria-labelledby="helloModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg">
      <div class="modal-content bg-pradera">
        <div class="modal-header">
          <h5 class="modal-title" id="helloModalLabel"><?php echo lang('MODAL_HELLO_TITLE'); ?></h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">

          <div class="alert alert-primary text-center m-2" role="alert">
            <h5 class="m-0">
              <a id="addressWsp" href="#" target="_blank" rel="noopener noreferrer">

              </a>
            </h5>
          </div>
          <button class="btn btn-primary" data-clipboard-target="#addressWsp" type="button"><?php echo lang('MODAL_HELLO_COPYBUTTON'); ?></button>


        </div>

      </div>
    </div>
  </div>

  <!-- Modal Compartir -->
  <div class="modal fade" id="shareModal" tabindex="-1" aria-labelledby="shareModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content bg-pradera">
        <div class="modal-header">
          <h5 class="modal-title" id="shareModalLabel"><?php echo lang('MODAL_SHARE_TITLE'); ?></h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
          ...
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-bs-dismiss="modal"><?php echo lang('CANCEL_BUTTON'); ?></button>
          <button type="button" class="btn btn-primary"><?php echo lang('MODAL_SHARE_BUTTON'); ?></button>
        </div>
      </div>
    </div>
  </div>

  <!-- Modal Editar -->
  <div class="modal fade" id="editModal" tabindex="-1" aria-labelledby="editModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg">
      <div class="modal-content bg-pradera">
        <div class="modal-header">
          <h5 class="modal-title" id="editModallabel"><?php echo lang('MODAL_EDIT_TITLE'); ?></h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
          <div class="card">
            <div class="card-body" id="body-modal-edit">

            </div>
            <div class="card-footer">
              <div class="alert alert-warning mb-0 text-center" role="alert">
                <?php echo lang('MODAL_EDIT_WARNING'); ?>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-bs-dismiss="modal"><?php echo lang('CANCEL_BUTTON'); ?></button>
          <button id="save-change" type="button" class="btn btn-primary save-change editar_registro"><?php echo lang('MODAL_EDIT_BUTTON'); ?></button>
        </div>
      </div>
    </div>
  </div>

  <!-- Modal Borrar -->
  <div class="modal fade" id="deleteModal" tabindex="-1" aria-labelledby="deleteModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg">
      <div class="modal-content bg-pradera">
        <div class="modal-header">
          <h5 class="modal-title" id="deleteModalLabel"><?php echo lang('MODAL_DELETE_TITLE'); ?></h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body"  id="body-modal-delete">
          
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-bs-dismiss="modal"><?php echo lang('CANCEL_BUTTON'); ?></button>
          <button type="button" class="btn btn-danger eliminar_registro"><?php echo lang('MODAL_DELETE_BUTTON'); ?></button>
        </div>
      </div>
    </div>
  </div>