
              <div class="row py-3 pt-0 border-bottom d-flex justify-content-center align-items-center">
                <div class="col-xl-3 col-lg-3 col-md-12 col-sm-12 col-12"><?php echo lang('MODAL_EDIT_PREFIX'); ?></div>
                <div class="col-xl-9 col-lg-9 col-md-12 col-sm-12 col-12"><select class="form-select" aria-label="Default select example">
                    <option selected value="1">wsp.chat</option>
                    <option value="" disabled>wspto.me (<?php echo lang('SOON'); ?>)</option>
                    <option value="" disabled>whtspp.me (<?php echo lang('SOON'); ?>)</option>
                  </select>
                </div>

              </div>

              <div class="row py-3 border-bottom  d-flex justify-content-center align-items-center">
                <div class="col-xl-3 col-lg-3 col-md-12 col-sm-12 col-12">
                <?php echo lang('TH_PHONE'); ?>
                </div>
                <div class="col-xl-9 col-lg-9 col-md-12 col-sm-12 col-12" id="phoneBIti">
                  <input id="phoneB" class="w-100" name="phoneB" type="tel" value="<?php echo "+".$compra[0]->codigo_numero.$compra[0]->numero_asociado;  ?>">
                </div>
              </div>

              <div class="row pt-3 pb-2 border-bottom d-flex justify-content-center align-items-center ">
                <div class="col-xl-3 col-lg-3 col-md-12 col-sm-12 col-12">
                <?php echo lang('TH_URL'); ?>
                </div>
                <div class="col-xl-9 col-lg-9 col-md-12 col-sm-12 col-12">

                  <div class="row">
                    <div class="col-xl-7 col-lg-7 col-md-12 col-sm-12 col-12">
                      <div class="d-flex justify-content-between align-items-center">
                        <input id="fieldB" onkeyup="countCharB(this)" type="text" maxlength="35" value="<?php echo $compra[0]->enlace;  ?>" />
                        <input id="id_compra_editar" type="number" value="<?php echo $compra[0]->id_compra;  ?>" hidden/>
                        <div id="charNumB" class="px-2"><?php echo (35 - strlen($compra[0]->enlace)); ?></div>
                      </div>
                      <div class="permalink-desk mb-2">
                        <small>wsp.chat/<span class="permalinkB"><?php echo $compra[0]->enlace;  ?></span></small>
                        <input id="fieldB-san" type="text" class="permalinkB" value="<?php echo $compra[0]->enlace;  ?>" hidden/>
                      </div>
                    </div>
                    <div class="col-xl-5 col-lg-5 col-md-12 col-sm-12 col-12">
                      <div class="check-available w-100 text-center">
                        <button type="button" class="btn btn-success btn-available mx-auto w-100" id="check-availability-edit"><?php echo lang('AVAILABILTY_BUTTON'); ?></button>
                      </div>
                    </div>
                    <div class="d-flex flex-column justify-content-center">
                
                    <div class="alert alert-success text-center mb-1 p-2" role="alert" style="display: none;" id="alert-disp-edit">
                  <?php echo lang('AVAILABILTY_YES'); ?>
                </div>


                <div class="alert alert-danger text-center mb-1 p-2" role="alert" style="display: none;" id="alert-vacio-edit">
                  <?php echo lang('EMPTY_FIELD'); ?>
                </div>
                <div class="alert alert-danger text-center mb-1 p-2" role="alert" style="display: none;" id="alert-nodisp-edit">
                  <?php echo lang('AVAILABILTY_NO'); ?>
                </div>
              </div>

                  </div>




                </div>
              </div>

              <div class="row pt-3 pb-2 d-flex justify-content-center align-items-center">
                <div class="col-xl-3 col-lg-3 col-md-12 col-sm-12 col-12">
                <?php echo lang('MODAL_EDIT_FORSALE'); ?>
                </div>
                <div class="col-xl-9 col-lg-9 col-md-12 col-sm-12 col-12">
                  <div class="form-check form-switch">
                    <input class="form-check-input" type="checkbox" id="flexSwitchCheckDisabled" disabled>
                    <label class="form-check-label" for="flexSwitchCheckDisabled"><?php echo lang('SOON'); ?></label>
                  </div>
                </div>
              </div>
