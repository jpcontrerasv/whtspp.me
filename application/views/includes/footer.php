<div class="w-100 text-center p-3">
  <p style="color: #BBB;"><small>wsp.chat | 2021 | <?php echo lang('FOOTER_MADEBY'); ?> <a href="http://jpcontreras.com" rel="noopener noreferrer" target="_blank" style="color: #BBB;"> Juan Pablo</a> <?php echo lang('AMPERSAND'); ?> <a href="https://www.linkedin.com/in/leroy-carrasquero-7ab659106/?originalSubdomain=ve" rel="noopener noreferrer" target="_blank" style="color: #BBB;">Leroy</a></small></p>
</div>
<div class="bloquear-fondo" id="loader-b" style="display:none">
  <div class="centered">
    <div class="spinner"></div>
  </div>
</div>
<!-- Optional JavaScript; choose one of the two! -->

<!-- Option 1: Bootstrap Bundle with Popper -->
<script src="<?php echo base_url('/public/js/bootstrap.bundle.min.js'); ?>"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<script src="<?php echo base_url('/public/js/replaceme.min.js'); ?>"></script>
<script src="<?php echo base_url('/public/js/utils.js'); ?>"></script>
<script src="<?php echo base_url('/public/js/intlTelInput.js'); ?>"></script>
<script src="<?php echo base_url('/public/js/clipboard.min.js'); ?>"></script>

<!-- Dependency SpeakingURL -->
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/speakingurl/6.0.0/speakingurl.min.js"></script>
<!-- stringToSlug -->
<script type="text/javascript" src="<?php echo base_url('/public/js/jquery.stringtoslug.js'); ?>"></script>

<script src="<?php echo base_url('/public/js/jquery.event.move.js'); ?>"></script>
<script src="<?php echo base_url('/public/js/jquery.twentytwenty.js'); ?>"></script>

<!-- montar minificado al terminar las funciones -->
<script src="<?php echo base_url('/public/js/scripts-min.js'); ?>?=<?php echo rand(5, 1000);?>"></script>

<!-- Option 2: Separate Popper and Bootstrap JS -->
<!--
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.3/dist/umd/popper.min.js" integrity="sha384-W8fXfP3gkOKtndU4JGtKDvXbO53Wy8SZCQHczT5FMiiqmQfUpWbYdTil/SxwZgAN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.min.js" integrity="sha384-skAcpIdS7UcVUC05LJ9Dxay8AXcDYfBJqt1CJ85S/CFujBsIzCIv+l9liuYLaMQ/" crossorigin="anonymous"></script>
    -->
</body>

</html>