<header class="site-header sticky-top py-1 bg-persian">
    <nav class="container-fluid d-flex flex-row align-items-center justify-content-between">
      <a class="py-2" href="<?php echo base_url(); ?>" aria-label="Product">
        <img src="<?php echo base_url('/public/img/whtsppmelogo.svg'); ?>" alt="wsp.chat logo">
      </a>
      <div class="menu">
        <a class="px-1" href="<?php echo base_url('idioma/english'); ?>"><img src="<?php echo base_url('/public/img/en.png'); ?>" title="English" /></a>
        <a class="px-1" href="<?php echo base_url('idioma/spanish'); ?>"><img src="<?php echo base_url('/public/img/es.png'); ?>" title="Spanish" /></a>
        <?php 
        if(isset($sesion)){  
        if($sesion['logged_in'] == 'true'){ ?>
   <a class="px-1" href="<?php echo base_url('usuario/logout') ?>"><?php echo lang('EXIT_DASHBOARD'); ?></a>
     <?php   }else{ ?>
          <a class="px-1" href="#" data-bs-toggle="modal" data-bs-target="#enterModal"><?php echo lang('LINK_DASHBOARD'); ?></a>
      <?php    }
         
       }else{ ?>
          <a class="px-1" href="#" data-bs-toggle="modal" data-bs-target="#enterModal"><?php echo lang('LINK_DASHBOARD'); ?></a>
        <?php }
        
        ?>
        
      </div>

    </nav>
  </header>