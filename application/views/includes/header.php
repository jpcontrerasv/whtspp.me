<?php
//var_dump($compras);
?>
<!doctype html>
<html dir="ltr" lang="<?php echo lang('LANG_DOC'); ?>" xml:lang="<?php echo lang('LANG_DOC'); ?>">

<head>
  <!-- Google Tag Manager -->
  <script>
    (function(w, d, s, l, i) {
      w[l] = w[l] || [];
      w[l].push({
        'gtm.start': new Date().getTime(),
        event: 'gtm.js'
      });
      var f = d.getElementsByTagName(s)[0],
        j = d.createElement(s),
        dl = l != 'dataLayer' ? '&l=' + l : '';
      j.async = true;
      j.src =
        'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
      f.parentNode.insertBefore(j, f);
    })(window, document, 'script', 'dataLayer', 'GTM-MQC37L4');
  </script>
  <!-- End Google Tag Manager -->

  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <meta http-equiv="content-language" content="<?php echo lang('LANG_DOC'); ?>" />

  <meta name="description" content="<?php echo lang('META_DESCRIPTION'); ?>" />
  <meta name="author" content="wsp.chat" />

  <!--favicon-->
  <link rel="apple-touch-icon" sizes="180x180" href="<?php echo base_url(''); ?>public/img/favicon/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="<?php echo base_url(''); ?>public/img/favicon/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url(''); ?>public/img/favicon/favicon-16x16.png">
  <link rel="manifest" href="<?php echo base_url(''); ?>public/img/favicon/site.webmanifest">
  <link rel="mask-icon" href="<?php echo base_url(''); ?>public/img/favicon/safari-pinned-tab.svg" color="#5bbad5">
  <meta name="msapplication-TileColor" content="#00aba9">
  <meta name="theme-color" content="#ffffff">

  <!--og-->
  <meta property="og:title" content="wsp.chat - <?php echo lang('PAGE_TITLE'); ?>" />
  <meta property="og:url" content="<?php echo base_url(''); ?>" />
  <meta property="og:type" content="website" />
  <meta property="og:image" content="<?php echo base_url(''); ?>public/img/lang-og/og-image-<?php echo lang('LANG_VAR'); ?>.png?=<?php echo rand(5, 1000); ?>" />
  <meta property="og:description" content="<?php echo lang('META_DESCRIPTION'); ?>" />

  <?php /*
  <meta name="twitter:site" content="@badweedx" />
  <meta name="twitter:creator" content="@badweedx" />
*/ ?>

  <!-- Bootstrap CSS -->
  <link href="<?php echo base_url('/public/css/twentytwenty.css'); ?>" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="<?php echo base_url('/public/css/intlTelInput.css'); ?>">
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
  <link href="<?php echo base_url('/public/css/bootstrap.min.css'); ?>" rel="stylesheet">
  <link href="<?php echo base_url('/public/css/custom-ler.css'); ?>" rel="stylesheet">
  <link href="<?php echo base_url('/public/css/custom-style.css'); ?>?=<?php echo rand(5, 1000); ?>" rel="stylesheet">

  <title>wsp.chat - <?php echo lang('PAGE_TITLE'); ?></title>
  <meta name="facebook-domain-verification" content="zv7q4oipnvotxqe5urtfwd7ubxdfvs" />

  <!--adsense-->
  <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-8612449333433270"
     crossorigin="anonymous"></script>

</head>

<body>
  <!-- Google Tag Manager (noscript) -->
  <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MQC37L4" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
  <!-- End Google Tag Manager (noscript) -->