<!-- Modal Entrar -->
<div class="modal fade" id="enterModal" tabindex="-1" aria-labelledby="enterModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content bg-pradera">
      <div class="modal-header">
        <h5 class="modal-title"><?php echo lang('MODAL_1_TITLE'); ?></h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <small class="mb-2 d-block"><?php echo lang('MODAL_1_INSTRUCTION'); ?></small>
        <input type="email" id="correo-user" class="w-100">
        
        <div id="ifMailExist" class="alert alert-success p-1 mt-2 d-none" role="alert" style="line-height: 1em;">
          <small><?php echo lang('MODAL_1_MAILEXIST'); ?></small>
        </div>

        <div id="ifMailEmpty" class="alert alert-danger p-1 mt-2 d-none" role="alert" style="line-height: 1em;">
          <small><?php echo lang('MODAL_1_MAILNOTENTERED'); ?></small>
        </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary send-log"><?php echo lang('MODAL_1_CTA'); ?></button>
      </div>
    </div>
  </div>
</div>
<!--hola-->