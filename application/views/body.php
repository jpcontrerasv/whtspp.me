<div class="container-fluid pt-4 mt-4">
  <div class="row">
    

    <!--left call-->
    <div class="col-xl-6 offset-xl-1 col-lg-7 col-md-12 col-sm-12 col-12 left-call">

      <h1 class="display-5 fw-normal mb-3">
        <?php echo lang('MAIN_TITLE'); ?>
      </h1>
      <h2 class="mb-4 fw-light"><?php echo lang('PRICE_LINE'); ?></h2>

      <!--comparación-->
      <div class="card mb-4 border-0">
        <div class="card-header text-center border-0">
          <?php echo lang('COMPARISION'); ?>
        </div>
        <div class="card-body p-0">
          <div class="twentytwenty-container">
            <img src="<?php echo base_url('/public/img/mockup@2x-color.png'); ?>" />
            <img src="<?php echo base_url('/public/img/mockup@2x-bw.png'); ?>" />
          </div>
        </div>
      </div> 

      <!--rotación de direcciones-->
      <div class="card bg-mystic px-4 border-0 mb-2 url-example d-flex">
        <div class="card-body d-flex align-items-center justify-content-center">
          <img src="<?php echo base_url('/public/img/ico-whatsapp.svg'); ?>" class="mr-5 d-block">&nbsp;&nbsp;&nbsp;<h2 class="m-0">wsp.chat/<span class="replace-me"><?php echo lang('EXAMPLES'); ?></span>
          </h2>
        </div>
      </div>

      


      <span class="d-block text-center w-100 mb-5 text-muted"><?php echo lang('SUB_EXAMPLE'); ?>.</span>
      <p><?php echo lang('DESC1'); ?></p>
      <ul class="list-inline">
        <li style="margin-right: 25px;" class="list-inline-item fs-5">✅ <?php echo lang('BENEFIT_1'); ?> &nbsp;</li>
        <li style="margin-right: 25px;" class="list-inline-item fs-5">✅ <?php echo lang('BENEFIT_2'); ?></li>
        <li style="margin-right: 25px;" class="list-inline-item fs-5">✅ <?php echo lang('BENEFIT_3'); ?></li>
      </ul>
      <!--test-->
     

      <?php /*
      
      <div class="card my-5">
           <?php 
           //print json_encode($result);
           
          // echo $result['languages']; ?>
      </div> 
      */?>
      
    </div>

    <!--right call-->
    <div id="your-link" class="col-xl-4 col-lg-5 col-md-12 col-sm-12 col-12">
      <h2 class="text-center mb-4 mt-2"><?php echo lang('TITLE_BLOCK_RIGHT'); ?></h2>
      <div class="card border-0 main-action mb-3">
        <div class="card-body">

          <!-- 1 -->
          <section class="border-light border-bottom pb-3 mb-2">
            <p class="fs-4"><?php echo lang('TITLE_WHATSAPP_NUMBER'); ?></p>
            <input id="phone" class="w-100" name="phone" type="tel">

          </section>

          <!-- 2 -->
          <section class="border-light mb-2">
            <p style="line-height: 1.1em;" class="fs-4"><?php echo lang('TITLE_CREATE_ADDRESS'); ?></p>

            <div class="row">

              <div class="col-xl-7 col-lg-7 col-md-12 col-sm-12 col-12">
                <input id="field" onkeyup="countChar(this)" type="text" class="phone-slug" maxlength="35" />
                <div id="charNum" class="px-2">35</div>
                <div class="permalink-desk">
                  <small>wsp.chat/<span class="permalink"></span></small>
                  <input id="field-san" type="text" class="permalink" value="" hidden/>
                </div>
              </div>
              
              <div class="col-xl-5 col-lg-5 col-md-12 col-sm-12 col-12">

                <div class="check-available w-100 text-center">
                  <button type="button" class="btn btn-success btn-available mx-auto w-100" id="check-availability"><?php echo lang('AVAILABILTY_BUTTON'); ?></button>
                </div>
                <div id="resp-paypal" style="display:none"></div>
              </div>

            </div>
            <!--
            <div class="permalink-desk">
              <small class="text-muted">wsp.chat/<span class="permalink"></span></small>
            </div>-->



            <div class="d-flex flex-column justify-content-center">
              <div class="alert alert-success text-center mb-1 p-2" role="alert" style="display: none;" id="alert-disp">
                <?php echo lang('AVAILABILTY_YES'); ?>
              </div>
              <div class="alert alert-danger text-center mb-1 p-2" role="alert" style="display: none;" id="alert-vacio">
                <?php echo lang('EMPTY_FIELD'); ?>
              </div>
              <div class="alert alert-danger text-center mb-1 p-2" role="alert" style="display: none;" id="alert-nodisp">
                <?php echo lang('AVAILABILTY_NO'); ?>
              </div>
            </div>


          </section>


          <?php /*
            
            <!--3-->
            <section class="pb-1 mb-1 pt-2 border-light border-top">
              <div class="mb-3 form-check">
                <input type="checkbox" class="form-check-input" id="checkMessage">
                <label class="form-check-label" for="checkMessage">&nbsp;&nbsp;<?php echo lang('INCLUDE_MESSAGE'); ?></label>
              </div>
              <div class="mb-3">
                <div class="form-group helloMessage">
                  <textarea class="form-control" placeholder="<?php echo lang('PLACEHOLDER_MESSAGE'); ?>" rows="3" maxlength="140"></textarea>
                </div>
              </div>
            </section>

            */ ?>


          <!-- 4 -->
          <section class="text-center pb-1 mb-1 mt-5">
            <button class="btn main-cta mb-3" disabled type="button">
              <?php echo lang('GET_IT_BUTTON'); ?>
            </button>
            <div class="logo-paypal w-100 text-muted">
              🔒 <?php echo lang('SAFE_PAYMENT'); ?> &nbsp; <img src="<?php echo base_url('/public/img/pplogo.svg'); ?>">
            </div>
            <?php /*
              <p class="m-0 text-muted" style="line-height: 1em;"><small><?php echo lang('GET_IT_DISCLAIMER'); ?></small> </p>
              */ ?>
          </section>





        </div>
      </div>

      <div class="card bg-mystic mb-5">
        <div class="card-body text-center">
        <?php echo lang('GOT_QUESTIONS'); ?><br> <a href="https://wsp.chat/<?php echo lang('GOT_QUESTIONS_HI'); ?>" target="_blank" rel="noopener noreferrer">wsp.chat/<?php echo lang('GOT_QUESTIONS_HI'); ?></a> 
        </div>
      </div>
    </div>

  </div>
</div>