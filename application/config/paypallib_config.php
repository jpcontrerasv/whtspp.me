<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

// ------------------------------------------------------------------------
// Paypal IPN Class
// ------------------------------------------------------------------------

// Use PayPal on Sandbox or Live
$config['sandbox'] = FALSE; // FALSE for live environment

// PayPal Business Email ID
//$config['business'] = 'sb-n473c15794256@business.example.com';
$config['business'] = 'wspxat@gmail.com';

// If (and where) to log ipn to file
$config['paypal_lib_ipn_log_file'] = BASEPATH . 'logs/paypal_ipn.log';
$config['paypal_lib_ipn_log'] = TRUE;

// Where are the buttons located at 
$config['paypal_lib_button_path'] = 'buttons';

// What is the default currency?
$config['paypal_lib_currency_code'] = 'USD';


/** SandBox Account Leroy
 * Email:
 * sb-n473c15794256@business.example.com
 * 
 * Username:
 * sb-n473c15794256_api1.business.example.com
 * 
 * Password:
 * P6NK4ZLL6CYW7XAH
 * 
 * Signature:
 * Ad8MOJ36cV85kWpl7rf4WydlpO61Atal8xpzUQNMXjmssUlcMleHB6yJ
 */
?>
