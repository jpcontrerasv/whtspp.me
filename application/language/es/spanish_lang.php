<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/* 
-----------------
Language: Spanish
-----------------
*/
//soon
$lang['SOON'] = 'Próximamente';
//lang
$lang['LANG_DOC'] = 'es';
$lang['LANG_VAR'] = 'esp';

//META
$lang['PAGE_TITLE'] = 'Crea una dirección de WhatsApp inolvidable';
$lang['META_DESCRIPTION'] = 'Úsalo para tu negocio, con tu propio nombre o tu empresa.';

//MENU
$lang['LINK_DASHBOARD'] = 'Entrar';
$lang['EXIT_DASHBOARD'] = 'Salir';

// LEFT SIDE
$lang['MAIN_TITLE'] = 'Cambia tu número de WhatsApp por una dirección <u>inolvidable</u>';

$lang['PRICE_LINE'] = 'Envía a tus clientes o nuevos contactos directo a tu conversación de WhatsApp, por sólo USD$2 anual.';

$lang['PRONOUN'] = 'tu';
$lang['EXAMPLES'] = 'tu-nombre,empresa.com,mi-tienda';
$lang['SUB_EXAMPLE'] = 'Puedes usarlo para tu tienda de Instagram, en la firma de tu correo personal, en LinkedIn, como link en las historias de instagram, en camiones repartidores, en bolsas, en packaging, en letreros, en vitrinas...';

$lang['DESC1'] = 'Los links personalizados son la herramienta perfecta para incrementar la cantidad de usuarios que llegan a tu chat y aumentar las ventas de tu negocio.';

$lang['BENEFIT_1'] = 'Fácil de recordar';
$lang['BENEFIT_2'] = 'Personal';
$lang['BENEFIT_3'] = 'Irrepetible';

$lang['COMPARISION'] = 'Desliza y compara';


// RIGHT BLOCK
$lang['TITLE_BLOCK_RIGHT'] = 'Crea tu link aquí';

$lang['TITLE_WHATSAPP_NUMBER'] = 'Tu número de WhatsApp';

$lang['TITLE_CREATE_ADDRESS'] = 'Escribe tu dirección personalizada';

$lang['AVAILABILTY_BUTTON'] = '¿Está Disponible?';

$lang['AVAILABILTY_YES'] = '¡Disponible!';
$lang['EMPTY_FIELD'] = 'Por favor, rellene los campos faltantes.';
$lang['AVAILABILTY_NO'] = 'No disponible. Intenta otro nombre.';

$lang['INCLUDE_MESSAGE'] = '¿Incluir mensaje de bienvenida?';
$lang['PLACEHOLDER_MESSAGE'] = '¡Hola! Quisiera saber más de...';

$lang['GET_IT_BUTTON'] = 'Obtenlo ahora por USD$2 <small>al año</small>';
$lang['SAFE_PAYMENT'] = 'Pago seguro con';
$lang['GET_IT_DISCLAIMER'] = 'Al registrarlo tendrás un panel donde podrás <br>editar la información de tu link en cualquier momento.';

$lang['GOT_QUESTIONS'] = '¿Tienes dudas? Conversemos';
$lang['GOT_QUESTIONS_HI'] = 'hola';


//DASHBOARD
$lang['MY_ADDRESSES'] = 'Mis direcciones personalizadas';

$lang['TH_COPY'] = 'Compartir';
$lang['TH_URL'] = 'URL';
$lang['TH_CLICKS'] = 'Visitas';
$lang['TH_PHONE'] = 'Teléfono';
$lang['TH_EXP'] = 'Expira';
$lang['TH_EDIT'] = 'Editar';
$lang['TH_DELETE'] = 'Borrar';

$lang['SIDE_TITLE_SECTION'] = 'Crea otra dirección aquí';

//Check email page
$lang['CHECKPAGE_TITLE'] = '¡Casi listos con tu nueva dirección de WhatsApp!';
$lang['CHECKPAGE_BODY'] = 'Revisa tu correo para confirmar tu cuenta.';
$lang['CHECKPAGE_FOOTER'] = 'Si no ves nada en tu bandeja de entrada, revisa tu carpeta de spam.';
$lang['BACK_TO_INDEX'] = 'Regresar a wsp.chat';

//MODALES

$lang['CANCEL_BUTTON'] = 'Cancelar';

//Modal Log In
$lang['MODAL_1_TITLE'] = 'Entrar';
$lang['MODAL_1_INSTRUCTION'] = 'Ingresa el mail utilizado en <strong>PayPal</strong> para comprar.';
$lang['MODAL_1_CTA'] = 'Enviar link de ingreso';
$lang['MODAL_1_MAILEXIST'] = 'Si el correo ingresado existe en nuestros registros, te enviaremos un link de acceso. <strong>Revisa tu bandeja de entrada y/o spam.</strong>';
$lang['MODAL_1_MAILNOTENTERED'] = 'Por favor, ingresa el correo utilizado con PayPal para comprar.';

//Modal Editar
$lang['MODAL_EDIT_TITLE'] = 'Editar';
$lang['MODAL_EDIT_BUTTON'] = 'Realizar cambios';
$lang['MODAL_EDIT_FORSALE'] = 'Poner a la venta';
$lang['MODAL_EDIT_PREFIX'] = 'Prefijo';
$lang['MODAL_EDIT_WARNING'] = 'Si cambias la dirección actual, perderás la anterior. <br>Procede con cuidado.';

//Modal Borrar
$lang['MODAL_DELETE_TITLE'] = 'Borrar';
$lang['MODAL_DELETE_BUTTON'] = 'Borrar';
$lang['MODAL_DELETE_WARNING'] = '¿Deseas borrar la dirección';

//Modal Compartir
$lang['MODAL_SHARE_TITLE'] = 'Compartir';
$lang['MODAL_SHARE_BUTTON'] = 'Compartir';

//Modal Hello
$lang['MODAL_HELLO_TITLE'] = 'Comparte tu dirección de WhatsApp';
$lang['MODAL_HELLO_COPYBUTTON'] = 'Copiar';

//Mail Buy
$lang['SUBJECT_MAIL_BUY'] = 'Compra realizada exitosamente';

//Footer
$lang['FOOTER_MADEBY'] = 'Hecho por';
$lang['AMPERSAND'] = '&';

?>