<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/* 
------------------
Language: English
------------------
*/
//soon
$lang['SOON'] = 'Coming Soon';

//lang
$lang['LANG_DOC'] = 'es';
$lang['LANG_VAR'] = 'esp';

//META
$lang['PAGE_TITLE'] = 'Crea una dirección de WhatsApp inolvidable';
$lang['META_DESCRIPTION'] = 'Úsalo para tu negocio, con tu propio nombre o tu empresa.';

//MENU
$lang['LINK_DASHBOARD'] = 'Log In';
$lang['EXIT_DASHBOARD'] = 'Log Out';

// LEFT SIDE
$lang['MAIN_TITLE'] = 'Change your WhatsApp number <br> for an <u>unmistakable</u> address';

$lang['PRICE_LINE'] = 'Directly to your WhatsApp conversation, for only USD$2 per year.';

$lang['PRONOUN'] = 'your';
$lang['EXAMPLES'] = 'my-name,my-company.com,my-store';
$lang['SUB_EXAMPLE'] = 'You can use it for your Instagram store, in the signature of your personal email, on LinkedIn, as a link in instagram stories, on delivery trucks, on bags, on packaging, on road billboards, on showcases...';

$lang['DESC1'] = 'Personalized links are the perfect tool to increase the number of users who come to your WhatsApp chat and increase 
the sales of your business.';

$lang['BENEFIT_1'] = 'Easy to remember';
$lang['BENEFIT_2'] = 'Personal';
$lang['BENEFIT_3'] = 'Unmistakable';

$lang['COMPARISION'] = 'Slide and compare';


// RIGHT BLOCK
$lang['TITLE_BLOCK_RIGHT'] = 'Create your link here';

$lang['TITLE_WHATSAPP_NUMBER'] = 'Your WhatsApp number';

$lang['TITLE_CREATE_ADDRESS'] = 'Enter your personalized name';

$lang['AVAILABILTY_BUTTON'] = 'Is it available?';

$lang['AVAILABILTY_YES'] = 'It is available!';
$lang['EMPTY_FIELD'] = 'Please, fill in the required fields.';
$lang['AVAILABILTY_NO'] = 'Not available. Try a different name.';

$lang['INCLUDE_MESSAGE'] = 'Include welcome message?';
$lang['PLACEHOLDER_MESSAGE'] = 'Hi there! I would like to know more about...';

$lang['GET_IT_BUTTON'] = 'Get it now for USD$2 <small>per year</small>';
$lang['SAFE_PAYMENT'] = 'Pay safely with';
$lang['GET_IT_DISCLAIMER'] = 'When registering you will access a dashboard<br> where you can edit your link information at any time.';

$lang['GOT_QUESTIONS'] = 'Got Questions? Chat with us';
$lang['GOT_QUESTIONS_HI'] = 'hi';


//DASHBOARD
$lang['MY_ADDRESSES'] = 'My Addresses';
$lang['TH_COPY'] = 'Share';
$lang['TH_URL'] = 'URL';
$lang['TH_CLICKS'] = 'Visitors';
$lang['TH_PHONE'] = 'Phone number';
$lang['TH_EXP'] = 'Due Date';
$lang['TH_EDIT'] = 'Edit';
$lang['TH_DELETE'] = 'Delete';

$lang['SIDE_TITLE_SECTION'] = 'Create a new address here';

//Check email page
$lang['CHECKPAGE_TITLE'] = 'Almost ready with your new WhatsApp address!';
$lang['CHECKPAGE_BODY'] = 'Check your Email to confirm your account.';
$lang['CHECKPAGE_FOOTER'] = 'If there is nothing on your inbox, please check your spam folder.';
$lang['BACK_TO_INDEX'] = 'Back to wsp.chat';


//MODALES

$lang['CANCEL_BUTTON'] = 'Cancel';

//Modal Log in
$lang['MODAL_1_TITLE'] = 'Log In';
$lang['MODAL_1_INSTRUCTION'] = 'Enter your registered email (from <strong>PayPal</strong> account)';
$lang['MODAL_1_CTA'] = 'Send log in link';
$lang['MODAL_1_MAILEXIST'] = 'If the entered Email exists in our records, we will send you an access link. <strong> Check your Inbox and/or Spam folder. </strong>';
$lang['MODAL_1_MAILNOTENTERED'] = 'Please enter the email you use with PayPal to buy.';

//Modal Editar
$lang['MODAL_EDIT_TITLE'] = 'Edit';
$lang['MODAL_EDIT_BUTTON'] = 'Edit address';
$lang['MODAL_EDIT_FORSALE'] = 'For Sale';
$lang['MODAL_EDIT_PREFIX'] = 'Prefix';
$lang['MODAL_EDIT_WARNING'] = 'If you change the current address, you will lose and release the previous one. <br>Proceed with caution.';

//Modal Borrar
$lang['MODAL_DELETE_TITLE'] = 'Delete';
$lang['MODAL_DELETE_BUTTON'] = 'Delete';
$lang['MODAL_DELETE_WARNING'] = 'Do you wish to delete the address';

//Modal Compartir
$lang['MODAL_SHARE_TITLE'] = 'Share';
$lang['MODAL_SHARE_BUTTON'] = 'Share';

//Modal Hello
$lang['MODAL_HELLO_TITLE'] = 'Share your WhatsApp address';
$lang['MODAL_HELLO_COPYBUTTON'] = 'Copy';

//Mail Buy
$lang['SUBJECT_MAIL_BUY'] = 'Buy success';

//Footer
$lang['FOOTER_MADEBY'] = 'Developed by';
$lang['AMPERSAND'] = '&';

?>