<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
class User_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
    /*
			Función para obtener data del enlace con el mismo como argumento.
		*/
    public function getDataEnlace($enlace)
    {
        //echo $enlace;
        //$array = array("dominio" => base_url(), "enlace" => $enlace, "id_status" => 1);
        /*$this->db->select('*');
        $this->db->from('wsp_compra');
        $this->db->where($array);*/
        $query = $this->db->query("SELECT * FROM wsp_compra WHERE dominio = '".base_url()."' AND BINARY enlace = '".$enlace."' AND id_status = 1");
        //$query = $this->db->get();
        //print_r($this->db->last_query());  
        //echo "<br>##################";
        //var_dump($query->result());
        return $query;
    }

    public function saveRequestBuy($data) // guardar compras
    {
        $query = $this->db->insert('wsp_compra', $data);
        return $this->db->insert_id();
    }
    public function saveUserRequest($data)  // guardar usuario
    {
        $query = $this->db->insert('wsp_user', $data);
        return $this->db->insert_id();
    }
    public function updateCompraIPN($status_paypal, $id_compra, $id_user)
    {
        $fecha_actual = date("Y-m-d H:i:s");
        //sumo 1 año
        $fecha_venc = date("Y-m-d H:i:s", strtotime($fecha_actual . "+1 year"));
        $data = array(
            'id_user' => $id_user,
            'paypal_status' => $status_paypal,
            'id_status' => 1,
            'f_compra' => $fecha_actual,
            'f_vence' => $fecha_venc
        );

        $this->db->where('id_compra', $id_compra);
        $this->db->update('wsp_compra', $data);
    }

    public function updateUserIPN($correo, $id_user)
    {
        $fecha_actual = date("Y-m-d H:i:s");
        $data = array(
            'correo_user' => $correo,
            'id_status' => 1
        );

        $this->db->where('id_user', $id_user);
        $this->db->update('wsp_user', $data);
    }


    public function getDataUser($array, $token = "")
    {
        if ($token != "") {
            $this->updateToken($token, $array["correo_user"]);
        }
        $this->db->select('*');
        $this->db->from('wsp_user');
        $this->db->where($array);
        $query = $this->db->get();
        return $query;
    }
    public function getDataUserLogin($array)
    {
        $this->db->select('*');
        $this->db->from('wsp_user');
        $this->db->where($array);
        $query = $this->db->get();
        // limpiamos el token de acceso.
        $this->updateTokenLogin("", $array["id_user"]);

        return $query;
    }

    private function updateToken($token, $correo)
    {
        //echo '<br>token obtener data usuario<br>' . $token. " :::: ". $correo;
        $this->db->set('token_login', md5($token));
        $this->db->where('correo_user', $correo);
        $this->db->where('id_status', 1);
        $this->db->update('wsp_user');
    }
    private function updateTokenLogin($token, $correo)
    {
        //echo '<br>token login<br>' . $token;
        $this->db->set('token_login', md5($token));
        $this->db->where('id_user', $correo);
        $this->db->where('id_status', 1);
        $this->db->update('wsp_user');
    }
}
