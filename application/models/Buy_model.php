<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
class Buy_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
    /*
			Función para obtener data del enlace con el mismo como argumento.
		*/
    public function getDataBuy($array)
    {

        $this->db->select('wsp_compra.id_compra,wsp_compra.id_user,wsp_compra.codigo_numero,wsp_compra.numero_asociado,wsp_compra.dominio,wsp_compra.enlace,wsp_compra.visitas,wsp_compra.f_vence,wsp_compra.id_status,wsp_compra.paypal_status');
        $this->db->from('wsp_compra');
        $this->db->join('wsp_user', 'wsp_user.id_user = wsp_compra.id_user');
        $this->db->where('wsp_compra.id_user', $array['id_user']);
        $this->db->where('wsp_compra.id_status', 1);
        $this->db->order_by('wsp_compra.id_compra','DESC');
        $query = $this->db->get();
        //print_r($this->db->last_query());
        return $query;
    }
    public function getDataBuyDetail($enlace)
    {

        $this->db->select('wsp_compra.id_compra,wsp_compra.id_user,wsp_compra.codigo_numero,wsp_compra.numero_asociado,wsp_compra.dominio,wsp_compra.enlace,wsp_compra.visitas,wsp_compra.f_vence,wsp_compra.id_status,wsp_compra.paypal_status');
        $this->db->from('wsp_compra');
        $this->db->join('wsp_user', 'wsp_user.id_user = wsp_compra.id_user');
        $this->db->where('wsp_compra.enlace', $enlace);
        $this->db->where('wsp_compra.id_status', 1);
        $query = $this->db->get();
        return $query;
    }
    public function getDataBuyId($array){
        $this->db->select('wsp_compra.id_compra,wsp_compra.id_user,wsp_compra.codigo_numero,wsp_compra.numero_asociado,wsp_compra.dominio,wsp_compra.enlace,wsp_compra.visitas,wsp_compra.f_vence,wsp_compra.id_status,wsp_compra.paypal_status');
        $this->db->from('wsp_compra');
        $this->db->join('wsp_user', 'wsp_user.id_user = wsp_compra.id_user');
        $this->db->where('wsp_compra.id_compra', $array["id_compra"]);
        $query = $this->db->get();
        return $query;
    }

    public function updateBuy($array, $id_compra = "")
    {   
        if($id_compra != ""){
            $this->db->where('id_compra', $id_compra);
        }
        $query = $this->db->update('wsp_compra', $array);
        return $query;
    }
    public function getDataEnlace($enlace, $id_compra)
    {
        /*$array = array("dominio" => base_url(), "enlace" => $enlace, "id_status" => 1, "id_compra != " =>$id_compra);
        $this->db->select('*');
        $this->db->from('wsp_compra');
        $this->db->where($array); */
        $query = $this->db->query("SELECT * FROM wsp_compra WHERE dominio = '".base_url()."' AND BINARY enlace = '".$enlace."' AND id_status = 1 AND id_compra != ".$id_compra);
        //$query = $this->db->get();
        return $query;
    }
    public function updateVisits($visits, $id_compra){
        $data = array(
            'visitas' => $visits
        );
        $this->db->where('id_compra', $id_compra);
        $this->db->update('wsp_compra', $data);
    }
}
