<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->view('welcome_message');
	}
	public function redirect_main(){
		header('Location: '.base_url());
	}
	
    public function not_found($url){
        
		if (is_null(get_cookie('LANGUAGE'))) {
			$this->lang->load('spanish_lang', 'es');
			$cookie = array(
				'name'   => 'LANGUAGE',
				'value'  => 'es-spanish_lang',
				'expire' => 604800
			);
			$this->input->set_cookie($cookie);
		} else {
			$idioma = explode('-', $this->input->cookie('LANGUAGE'));
			if (count($idioma) == 0) {
				$this->lang->load('spanish_lang', 'es');
			} else {
				$this->lang->load($idioma[1], $idioma[0]);
			}
		}
		$data["url"] = $url;
		$this->load->view('includes/header');
		//$this->load->view('includes/nav-header');
		$this->load->view('404',$data);
		//$this->load->view('includes/modal-login');
		$this->load->view('includes/footer');
}
}
