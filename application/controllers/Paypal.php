<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Paypal extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->library('paypal_lib');
        $this->load->model('user_model');
        if (is_null(get_cookie('LANGUAGE'))) {
            $this->lang->load('spanish_lang', 'es');
            $cookie = array(
                'name'   => 'LANGUAGE',
                'value'  => 'es-spanish_lang',
                'expire' => 604800
            );
            $this->input->set_cookie($cookie);
        } else {
            $idioma = explode('-', $this->input->cookie('LANGUAGE'));
            if (count($idioma) == 0) {
                $this->lang->load('spanish_lang', 'es');
            } else {
                $this->lang->load($idioma[1], $idioma[0]);
            }
        }
        //echo "en controlador paypal";
        //$this->load->model('user_model');   //carga un modelo con el nombre Main_model
    }

    public function index()
    {
        //echo "en controlador paypal";
        $enlace = $this->input->post('enlace');
        $codigo = $this->input->post('codigo');
        $numero = $this->input->post('numero');
        $monto = 2;

        if ($enlace != "") {
            $this->load->helper('badwords_helper');
            $is_bad = detectBadWords($enlace);
            //echo $is_bad;
            if ($is_bad == 'false') {
                // se recogen valores para hacer la inserción en las respectivas tablas (compra y usuario).
                $token = mt_rand(10000, 99999);
                // array para usuario
                /* $data_user = array(
                'created_at' => date('Y-m-d H:i:s'),
                'token_login' => md5($token),
                'id_rol' => 2,
                'id_status' => 2
            ); */
                $id_usuario = 0;
                if ($this->session->userdata('id_usuario')) {
                    $id_usuario = $this->session->userdata('id_usuario');
                }

                $id_user = $id_usuario;
                // array para compra
                $data = array(
                    'id_user' => $id_user,
                    'codigo_numero' => $codigo,
                    'numero_asociado' => $numero,
                    'dominio' => base_url(),
                    'enlace' => strtolower($enlace),
                    'costo_producto' => $monto,
                    'id_status' => 4
                );
                $id_compra = $this->user_model->saveRequestBuy($data);

                // array para sesión
                /*$data_sesion = array(
                'logged_in' => 'true'
            );
            $this->session->set_userdata($data_sesion); */

                //terminada la inserción y datos de sesión se procede a ir al pago y se lleva el id insertado de una vez.-
                $this->pay($enlace, $monto, $id_compra, $id_user);
            } else {
                echo "vacio";
                header('Location: ' . base_url());
            }
        } else {
            echo "vacio";
            header('Location: ' . base_url());
        }
    }

    private function pay($enlace, $monto, $id_compra = "", $id_user = "")
    {
        // obtener idioma actual 
        if($enlace != ""){
        $idioma = $this->input->cookie('LANGUAGE');
        //Set variables for paypal form
        $returnURL = base_url() . 'paypal/success'; //payment success url
        $cancelURL = base_url() . 'paypal/cancel'; //payment cancel url
        $notifyURL = base_url() . 'paypal/ipn'; //ipn url

        $this->paypal_lib->add_field('return', $returnURL);
        $this->paypal_lib->add_field('cancel_return', $cancelURL);
        $this->paypal_lib->add_field('notify_url', $notifyURL);
        $this->paypal_lib->add_field('item_name', $enlace);
        $this->paypal_lib->add_field('custom', $id_compra . "-" . $id_user . "-" . $idioma);
        $this->paypal_lib->add_field('item_number',  md5($id_compra));
        $this->paypal_lib->add_field('amount',  $monto);
        $this->paypal_lib->paypal_auto_form();
        }else{
            header("Location: ".base_url());
        }
    }

    public function success()
    {
        
        if ($this->session->has_userdata('id_usuario')) {
            header('Location: ' . base_url());
        } else {
            $this->load->view('includes/header');
            //$this->load->view('includes/nav-header');
            $this->load->view('check-email');
            $this->load->view('includes/modal-login');
            $this->load->view('includes/footer');
        }

        //echo "exitoso";
        //
    }

    public function cancel()
    {

        //echo "cancelado";
        if ($this->session->has_userdata('id_usuario')) {
            header('Location: ' . base_url());
        } else {
            $this->session->sess_destroy();
            header('Location: ' . base_url());
        }
    }

    function ipn()
    {
        //paypal return transaction details array
        $paypalInfo    = $this->input->post();
        $id_compra = explode("-", $paypalInfo['custom'])[0]; // id compra para actualizar el status paypl de la compra
        $status_paypal  = $paypalInfo["payment_status"]; // status retornado por paypal

        $id_user = explode("-", $paypalInfo['custom'])[1]; // id usuario para actualizar el correo del usuario y setearlo en la variable de sesión
        $correo_pagador = strtolower($paypalInfo["payer_email"]); // correo del usuario

        $flag = explode("-", $paypalInfo['custom'])[2];
        $archivo = explode("-", $paypalInfo['custom'])[3];

        $paypalURL = $this->paypal_lib->paypal_url;
        $result    = $this->paypal_lib->curlPost($paypalURL, $paypalInfo);

        //check whether the payment is verified
        if (preg_match("/VERIFIED/i", $result)) {
            //insert the transaction data into the database
            $token = mt_rand(10000, 99999);
            $data_user = array(
                'correo_user' => strtolower($correo_pagador),
                'id_status' => 1
            );
            $query = $this->user_model->getDataUser($data_user, $token);
            if ($query->num_rows() == 0) {
                $usuario = array(
                    'correo_user' => $correo_pagador,
                    'created_at' => date('Y-m-d H:i:s'),
                    'id_rol' => 2,
                    'id_status' => 1
                );

                $id_user_inserted = $this->user_model->saveUserRequest($usuario);
                $id_user = $id_user_inserted;
            } else {
                foreach ($query->result() as $user) {
                    $id_user = $user->id_user;
                }
            }

            $this->user_model->updateCompraIPN($status_paypal, $id_compra, $id_user);

            $this->lang->load($archivo, $flag);
            $this->sendMail($correo_pagador, $this->lang->line('SUBJECT_MAIL_BUY'), "Puedes ver tu nuevo enlace haciendo click acá: <a href='" . base_url() . "dashboard/login/" . $token . "-" . $id_user . "'> Ir al panel.</a>");
            /*foreach ($query->result() as $user) {
                $array = array(
                    'id_usuario' => $id_user,
                    'correo'  => strtolower($correo_pagador),
                    'created_at' => $user->created_at,
                    'id_rol' => $user->id_rol,
                    'id_status' => $user->id_status
                );
             } */
            //$this->session->set_userdata($array);

            //$this->session->set_userdata('correo', $correo_pagador);

        }
    }

    private function sendMail($to, $subject, $mensaje)
    {


        $config['protocol']    = 'smtp';
        $config['smtp_host']    = 'ssl://mail.wsp.chat';
        $config['smtp_port']    = '465';
        $config['smtp_timeout'] = '7';
        $config['smtp_user']    = 'hi@wsp.chat';
        $config['smtp_pass']    = 'OM5F&jLld{6P';
        $config['charset']    = 'utf-8';
        $config['newline']    = "\r\n";
        $config['mailtype'] = 'html'; // or html
        $config['validation'] = TRUE; // bool whether to validate email or not      

        $this->email->initialize($config);

        $this->email->from('hi@wsp.chat', 'wsp.chat');
        $this->email->to($to);
        $this->email->subject($subject);
        $this->email->message($mensaje);
        $this->email->send();
        //var_dump($this->email->send());
        //send mail data

    }
}
