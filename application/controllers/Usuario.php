<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Usuario extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('user_model');   //carga un modelo con el nombre Main_model
        $this->load->model('buy_model');   //carga un modelo con el nombre Main_model
        if (is_null(get_cookie('LANGUAGE'))) {
			$this->lang->load('spanish_lang', 'es');
			$cookie = array(
				'name'   => 'LANGUAGE',
				'value'  => 'es-spanish_lang',
				'expire' => 604800
			);
			$this->input->set_cookie($cookie);
		} else {
			$idioma = explode('-', $this->input->cookie('LANGUAGE'));
			if (count($idioma) == 0) {
				$this->lang->load('spanish_lang', 'es');
			} else {
				$this->lang->load($idioma[1], $idioma[0]);
			}
		}
    }

    public function index()
    {
        //$this->spanish();
        //var_dump($this->input->post("enlace"));
        
        //header('Content-Type: text/html; charset=UTF-8');
        $enlace = $this->input->post("enlace");
        //echo $enlace;
            //echo utf8_decode($enlace.  '  llega y conviert enlace<br> ñ   --   ñ' );
        $this->get_disponibilidad($enlace);
    }

    private function get_disponibilidad($enlace)
    {
        //echo $enlace ." :::get";
        $this->load->helper('badwords_helper');
        $is_bad = detectBadWords($enlace);
        //echo $is_bad;
        if ($is_bad == 'false') {
            $result_enlace = $this->user_model->getDataEnlace($enlace);
            //echo "<br>###<br>";
            //var_dump($result_enlace->num_rows());
            if ($result_enlace->num_rows() > 0) {
                echo "no-disponible";
            } else {
                echo "disponible";
            }
        } else {
            echo "no-disponible";
        }
        //header("location: https://api.whatsapp.com/send?phone=56963170022&text=%F0%9F%98%97%20blablabla");
    }

    public function get_info($enlace)
    {
        //echo $enlace . ' -- Listo para consultar en el modelo ';
        /*$data_user = array(
            'enlace' => $enlace,
            'id_status' => 1
        ); */
        $word = urldecode($enlace);
        $data_returned = $this->buy_model->getDataBuyDetail($word);
        //echo "data: " . $data_returned->num_rows() . '<br>';
        if ($data_returned->num_rows() > 0) {
            foreach ($data_returned->result() as $user) {
                $codigo = $user->codigo_numero;
                $numero = $user->numero_asociado;
                $visitas = $user->visitas;
                $id_compra = $user->id_compra;
            }
            $visitas = $visitas + 1;
            // aumentar contador de visitas
            $this->buy_model->updateVisits($visitas, $id_compra);
            header("location: https://api.whatsapp.com/send?phone=" . $codigo . $numero);
            //echo "<br>id usuario: " . $id_user . '<br>';
        } else {

            //header("location: " . base_url());
            redirect("welcome/not_found/".$word);
            //$this->not_found();
        }
        //header("location: https://api.whatsapp.com/send?phone=56963170022&text=%F0%9F%98%97%20blablabla");
    }
    public function login()
    {
        //echo "controlador login" . $this->input->post('correo');
        $correo = $this->input->post('correo');
        //$this->sendMail("hola", "hola", "hola");
        $this->getUserData($correo);
    }

    private function getUserData($correo)
    {
        $token = mt_rand(10000, 99999);
        // array para usuario
        $data_user = array(
            'correo_user' => $correo,
            'id_status' => 1
        );
        $data_returned = $this->user_model->getDataUser($data_user, $token);
        //echo "data: " . $data_returned->num_rows() . '<br>';
        if ($data_returned->num_rows() > 0) {
            $id_user = 0;
            foreach ($data_returned->result() as $user) {
                $id_user = $user->id_user;
            }
            //echo "<br>id usuario: " . $id_user . '<br>';
            $this->sendMail($correo, "Solicitud de acceso al sistema", "Enlace de acceso: <a href='" . base_url() . "dashboard/login/" . $token . "-" . $id_user . "'> Ingresa a tu perfil haciendo click acá.</a>");
        }
        var_dump($data_returned->result());
    }

    private function sendMail($to, $subject, $mensaje)
    {


        $config['protocol']    = 'smtp';
        $config['smtp_host']    = 'ssl://mail.wsp.chat';
        $config['smtp_port']    = '465';
        $config['smtp_timeout'] = '7';
        $config['smtp_user']    = 'hi@wsp.chat';
        $config['smtp_pass']    = 'OM5F&jLld{6P';
        $config['charset']    = 'utf-8';
        $config['newline']    = "\r\n";
        $config['mailtype'] = 'html'; // or html
        $config['validation'] = TRUE; // bool whether to validate email or not      

        $this->email->initialize($config);

        $this->email->from('hi@wsp.chat', 'wsp.chat');
        $this->email->to($to);
        $this->email->subject($subject);
        $this->email->message($mensaje);
        $this->email->send();
        //var_dump($this->email->send());
        //send mail data

    }

    public function logout()
    {
        $this->session->sess_destroy();
        header('Location: ' . base_url());
    }
}
