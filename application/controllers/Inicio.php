<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Inicio extends CI_Controller
{

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->library('geonames');

		$code = $this->geo();
		if($code == ""){
			$code = "US";
		}
		$results = $this->geonames->countryInfo(array('country'=>$code,'lang'=>'en'));
		//echo $results['geonames'][0]->countryName;
		$data["result"] = $results;
		//print_r($results);
		if (is_null(get_cookie('LANGUAGE'))) {
		foreach ($data["result"] as $d) {
			//var_dump($d);
			//var_dump($d);
			foreach($d as $data){
			  //echo $data->languages;
			  $check_lang_es = (strpos($data->languages, 'es') === false) ? 1 : 0;
			  $main_language = substr($data->languages, 0, 2);
			 //echo "<br>main language: ".$main_language.'<br>';
			  if( $main_language === 'es'){
			   //echo "si español";

			   $this->lang->load('spanish_lang', 'es');
			$cookie = array(
				'name'   => 'LANGUAGE',
				'value'  => 'es-spanish_lang',
				'expire' => 604800
			);
			$this->input->set_cookie($cookie);
			  }else{
			   //echo "no español";
			   
			   $this->lang->load('english_lang', 'en');
			$cookie = array(
				'name'   => 'LANGUAGE',
				'value'  => 'en-english_lang',
				'expire' => 604800
			);
			$this->input->set_cookie($cookie);
			  }
			}
		  }
		}else{
			$idioma = explode('-', $this->input->cookie('LANGUAGE'));
			if (count($idioma) == 0) {
				$this->lang->load('spanish_lang', 'es');
			} else {
				$this->lang->load($idioma[1], $idioma[0]);
			}
		}
		/*if (is_null(get_cookie('LANGUAGE'))) {
			$this->lang->load('spanish_lang', 'es');
			$cookie = array(
				'name'   => 'LANGUAGE',
				'value'  => 'es-spanish_lang',
				'expire' => 604800
			);
			$this->input->set_cookie($cookie);
		} else {
			$idioma = explode('-', $this->input->cookie('LANGUAGE'));
			if (count($idioma) == 0) {
				$this->lang->load('spanish_lang', 'es');
			} else {
				$this->lang->load($idioma[1], $idioma[0]);
			}
		} */
	}
	public function _remap($method)
	{
		//var_dump($this->uri);
		//echo "xd ".$this->uri->uri_string;
		if (strtolower($this->uri->uri_string) == '') {
			// determinar aquí la sesión iniciada para redirigir sea al dashboard o al index
			if ($this->session->has_userdata('logged_in')) {
				redirect('dashboard');
			} else {
				$this->get_index();
			}
		} else {

			$this->get_val($this->uri->uri_string);
		}
	}

	public function get_val($code)
	{

		//header("location: https://api.whatsapp.com/send?phone=56963170022&text=%F0%9F%98%97%20blablabla");
		redirect('usuario/get_info/' . $code);
	}
	public function get_index()
	{
		if ($this->session->has_userdata('logged_in')) {
			redirect('dashboard');
		} else {

			$this->load->view('includes/header');
			$this->load->view('includes/nav-header');
			$this->load->view('body');
			$this->load->view('includes/modal-login');
			$this->load->view('includes/footer');
		}
	}
	
public function geo(){
    $ip = $this->input->ip_address();
    $dataCountry = $this->getIpInfo($ip);
    //echo "CODE COUNTRY::: ".$dataCountry->country." ::::<br>";
	//var_dump($dataCountry);
	//var_dump($dataCountry);
	if(isset($dataCountry->country)){
		$code = $dataCountry->country;
	}else{
		$code = "";
	}
	
	return $code;
}
private function getIpInfo($ip){
    $curlSession = curl_init();
    curl_setopt($curlSession, CURLOPT_URL, 'https://ipinfo.io/'.$ip.'/?token=e481bc6d183087');
    curl_setopt($curlSession, CURLOPT_BINARYTRANSFER, true);
    curl_setopt($curlSession, CURLOPT_RETURNTRANSFER, true);

    $jsonData = json_decode(curl_exec($curlSession));
    curl_close($curlSession);
    return $jsonData;
}
	/*public function get_dashboard()
	{

		$this->load->view('includes/header');
		$this->load->view('includes/nav-header');
		$this->load->view('body');
		$this->load->view('includes/modal-login');
		$this->load->view('includes/footer');
	} */
}
