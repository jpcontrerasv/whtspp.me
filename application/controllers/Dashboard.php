<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		if (is_null(get_cookie('LANGUAGE'))) {
			$this->lang->load('spanish_lang', 'es');
			$cookie = array(
				'name'   => 'LANGUAGE',
				'value'  => 'es-spanish_lang',
				'expire' => 604800
			);
			$this->input->set_cookie($cookie);
		} else {
			$idioma = explode('-', $this->input->cookie('LANGUAGE'));
			if (count($idioma) == 0) {
				$this->lang->load('spanish_lang', 'es');
			} else {
				$this->lang->load($idioma[1], $idioma[0]);
			}
		}
	}

	public function index()
	{
		//$this->spanish();
		//var_dump($this->input->post("enlace"));
		//echo "gg";
		if ($this->session->has_userdata('logged_in')) {
			// cargamos el model
			$this->load->model('user_model');
			$this->load->model('buy_model');
			// montamos la consulta de los datos
			//var_dump($this->session->userdata);
			$datos = $this->getDataUsuario();
			$compras = $this->getBuyUsuario();
			$data['sesion'] = $this->session->userdata;
			$data["compras"] = $compras->result();
			$data["usuario"] = $datos->result();
			//var_dump($data);
			$this->load->view('includes/header', $data);
			$this->load->view('includes/nav-header');
			$this->load->view('body_dashboard');
			//$this->load->view('includes_dashboard/modal-compartir');
			$this->load->view('includes_dashboard/footer_dashboard');
		} else {
			header("Location: " . base_url());
		}

		//$this->session->sess_destroy();
	}
	public function login($token)
	{
		// validar si la sesión está seteada
		$id_user = explode("-", $token)[1];
		$token = md5(explode("-", $token)[0]);
		$this->load->model('user_model');

		$data_user = array(
			'id_user' => $id_user,
			'token_login' => $token,
			'id_status' => 1
		);
		$data_returned = $this->user_model->getDataUserLogin($data_user);
		//echo "data: " . $data_returned->num_rows() . '<br>';
		if ($data_returned->num_rows() > 0) {
			$array;
			foreach ($data_returned->result() as $user) {
				// array para sesión
				$array = array(
					'id_usuario' => $user->id_user,
					'correo'  => $user->correo_user,
					'created_at' => $user->created_at,
					'id_rol' => $user->id_rol,
					'id_status' => $user->id_status,
					'logged_in' => 'true'
				);
			}

			//echo "<br>id usuario: " . $id_user . '<br>';
			$this->session->set_userdata($array);
			header('Location: ' . base_url() . 'dashboard');
		} else {
			//echo "no-existe";
			header('Location: ' . base_url());
		}
		//var_dump($data_returned->result());
	}

	private function getDataUsuario()
	{
		$id_usuario = $this->session->userdata('id_usuario');
		$id_status = $this->session->userdata('id_status');

		$array = array(
			'id_user' => $id_usuario,
			'id_status' => $id_status
		);
		$data = $this->user_model->getDataUser($array);
		return $data;
	}

	private function getBuyUsuario()
	{
		$id_usuario = $this->session->userdata('id_usuario');
		$id_status = $this->session->userdata('id_status');
		$array = array(
			'id_user' => $id_usuario,
			'id_status' => $id_status
		);
		$data = $this->buy_model->getDataBuy($array);
		return $data;
	}
	public function getBuyId()
	{
		$this->load->model('buy_model');
		$id_compra = $this->input->post('id_compra');
		$array = array('id_compra' => $id_compra);
		$data = $this->buy_model->getDataBuyId($array);
		$compra["compra"] = $data->result();
		//var_dump($compra["compra"]);
		$vista = $this->load->view("body_modal_edit", $compra);
		return $vista;
	}
	public function getBuyIdDelete()
	{
		$this->load->model('buy_model');
		$id_compra = $this->input->post('id_compra');
		$array = array('id_compra' => $id_compra);
		$data = $this->buy_model->getDataBuyId($array);
		$compra["compra"] = $data->result();
		//var_dump($compra["compra"]);
		$vista = $this->load->view("body_modal_delete", $compra);
		return $vista;
	}
	public function editBuy()
	{
		$this->load->model('buy_model');
		$id_compra = $this->input->post('id_compra');
		$enlace = $this->input->post('enlace');
		$codigo = $this->input->post('codigo');
		$numero = $this->input->post('numero');
		$data = array(
			'codigo_numero' => $codigo,
			'numero_asociado' => $numero,
			'enlace' => $enlace
		);
		$upd = $this->buy_model->updateBuy($data, $id_compra);
		echo $upd;
	}
	public function deleteBuy()
	{
		$this->load->model('buy_model');
		$id_compra = $this->input->post('id_compra');
		$data = array(
			'id_status' => 3
		);
		$upd = $this->buy_model->updateBuy($data, $id_compra);
		echo $upd;
	}
	public function getDisponibilidad()
	{
		$this->load->model('buy_model');
		//$id_user = $this->session->userdata('id_usuario');
		//var_dump($this->session->userdata);
		$enlace = $this->input->post('enlace');
		$id_compra = $this->input->post('id_compra');
		$result_enlace = $this->buy_model->getDataEnlace($enlace, $id_compra);
		if ($result_enlace->num_rows() > 0) {
			echo "no-disponible";
		} else {
			echo "disponible";
		}
	}
}
