<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Idioma extends CI_Controller{
    function __construct(){
        parent::__construct();
    }

    public function index(){
        $this->spanish();
    }

    public function english(){
        //$this->lang->load('english_lang', 'en');
        //$this->session->set_userdata("LANGUAGE","english");
        $cookie = array(
                    'name'   => 'LANGUAGE',
                    'value'  => 'en-english_lang',
                    'expire' => 604800
                );
        $this->input->set_cookie($cookie);
        header("location: ".base_url());
    }

    public function spanish(){
        //$this->lang->load('spanish_lang', 'es');
        //$this->session->set_userdata("LANGUAGE","english");
        $cookie = array(
                    'name'   => 'LANGUAGE',
                    'value'  => 'es-spanish_lang',
                    'expire' => 604800
                );
        $this->input->set_cookie($cookie);
        header("location: ".base_url());
    }

    /*public function chinese(){
        $this->lang->load('labels', 'chinese');
        //$this->session->set_userdata("LANGUAGE","chinese");
        $cookie = array(
                    'name'   => 'LANGUAGE',
                    'value'  => 'chinese',
                    'expire' => 604800
                );
        $this->input->set_cookie($cookie);
        header("location: ".$_GET["ref"]);
    } */

}



?>
